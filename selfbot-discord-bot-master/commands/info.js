const { Command } = require('smooth-discord.js');
const { findUser, error } = require('../util.js');
const moment = require('moment');
const { stripIndents } = require('common-tags');
const Discord = require('discord.js');
const { embedColour } = require('../info.json');

module.exports = class InfoCommand extends Command {
	constructor() {
		super({
			name: 'info',
			description: 'Gets information on a user.',
			guildOnly: true,
			aliases: ['userinfo', 'ui'],
			perms: ['EMBED_LINKS']
		});
	}

	run(message, args) {
		const user = findUser(message, args);

		if (user === null) {
			return error('Please input a valid user', message);
		} else {
			const embed = new Discord.RichEmbed();
			const member = message.guild.members.get(user.id);
			if (member === undefined) return error('User is in a different server', message);
			const username = member.user.tag;
			const joinedAt = moment.utc(member.joinedTimestamp).format('MMM Do YYYY');
			const guildName = message.guild.name;
			const presence = user.presence.game ? user.presence.game.name : 'Nothing';
			const roles = member.roles.reduce((prev, next) => `${prev}, ${next}`);

			embed.setThumbnail(user.avatarURL)
			.setColor(embedColour)
			.addField('User Details', stripIndents`
				**Username** ${username}
				**Joined:** ${joinedAt}
				**Guild:** ${guildName}
				**Playing: **${presence}
				**Roles: ** ${roles}
			`);

			return message.edit({ embed });
		}
	}
};
