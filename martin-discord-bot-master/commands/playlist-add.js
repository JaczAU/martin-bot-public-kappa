const { Command } = require('smooth-discord.js');
const Playlist = require('../models/Playlist.js');
const token = require('../info.json').keys.google;
const YouTube = require('simple-youtube-api');
const youtube = new YouTube(token);
const { RichEmbed } = require('discord.js');
const { error } = require('../util.js');
const { stripIndents } = require('common-tags');

module.exports = class PlaylistAddCommand extends Command {
	constructor() {
		super({
			name: 'playlist-add',
			description: 'Add a song to your personal playlist.'
		});
	}

	run(message, args) {
		if (!args) return error('Please specify playlist and song.', message);

		const playlistName = args.split(' ')[0].toLowerCase();
		const songName = args.split(' ').slice(1).join(' ');
		
		if (!songName) return error('No song specified', message);

		youtube.searchVideos(songName, 1).then(vid => {
			if (!vid[0]) return error('No video was found.', message);
			youtube.getVideoByID(vid[0].id).then(res => {
				const url = `https://www.youtube.com/watch?v=${res.id}`;

				const songObj = {
					songName: res.title,
					songURL: url,
					requestedBy: message.author.id,
					length: `${res.duration.minutes}:${res.duration.seconds < 10 ? `0${res.duration.seconds}` : res.duration.seconds} minutes`,
					thumbNail: `https://img.youtube.com/vi/${res.id}/mqdefault.jpg`
				};

				Playlist.findOne({ where: { name: playlistName } }).then(playlist => {
					if (playlist) {
						if (playlist.authorId === message.author.id) {
							const array = JSON.parse(playlist.songsArray);
							if (array.length === 30) return error('You\'ve reached your maximum songs for your playlist. Please delete a song to add more.', message);
							array.push(songObj);
							playlist.songsArray = JSON.stringify(array);
							playlist.save().then(() => {
								const embed = new RichEmbed()
									.setColor(0x722aff)
									.setDescription(stripIndents`
										Added **${addSuffix(array.length)}** song to **${playlistName}**
										**Song:** ${res.title}
										**Length:** ${res.duration.minutes}:${res.duration.seconds < 10 ? `0${res.duration.seconds}` : res.duration.seconds} minutes
									`)
									.setThumbnail(`https://img.youtube.com/vi/${res.id}/mqdefault.jpg`);
								message.channel.send({ embed });
							});
						} else {
							error('You cannot edit someone elses playlist.', message);
						}
					} else {
						error(`There is no playlist called ${playlistName}`, message);
					}
				});
			});
		});
	}
};

function addSuffix(i) {
	const j = i % 10, k = i % 100;
	if (j === 1 && k !== 11) return `${i}st`;
	if (j === 2 && k !== 12) return `${i}nd`;
	if (j === 3 && k !== 13) return `${i}rd`;
	return `${i}th`;
}
