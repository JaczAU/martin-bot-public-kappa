const { Command } = require('smooth-discord.js');
const { error } = require('../util.js');

module.exports = class UnPauseCommand extends Command {
	constructor() {
		super({
			name: 'unpause',
			description: 'UnPauses the current song.',
			guildOnly: true,
			aliases: ['resume']
		});
	}

	run(message) {
		const voiceChannel = message.guild.me.voiceChannel;
		if (!voiceChannel) return error('I can\'t pause songs if i\'m not in a voice channel.', message);
		if (!voiceChannel.members.has(message.author.id)) return error('You\'re not in the voice channel.', message);

		if (message.guild.voiceConnection.dispatcher) {
			if (!message.guild.voiceConnection.dispatcher.paused) {
				message.channel.send('I\'m already unpaused. Use pause to stop the music.');
			} else {
				message.channel.send('Resuming...');
				message.guild.voiceConnection.dispatcher.resume();
			}
		} else {
			error('I\'m not playing anything?', message);
		}
	}
};
