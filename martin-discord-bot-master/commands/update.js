const { Command } = require('smooth-discord.js');
const { exec } = require('child_process');
const { URL } = require('url');
const info = require('../info.json');

const gitURL = new URL(info.gitURL);
gitURL.password = info.gitPassword;
gitURL.username = info.gitUsername;

module.exports = class UpateCommand extends Command {
	constructor() {
		super({
			name: 'update',
			description: 'Updates martin from the github repo.',
			ownerOnly: true,
			aliases: ['sync']
		});
	}

	run(message) {
		exec(`git pull ${gitURL}`, (err, stdout) => {
			if (err) return message.channel.send(err.message, { code: true });
			return message.channel.send(stdout, { code: 'fix' }).then(() => {
				process.exit();
			});
		});
	}
};
