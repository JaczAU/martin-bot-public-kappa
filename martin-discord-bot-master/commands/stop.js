const { Command } = require('smooth-discord.js');
const { error } = require('../util.js');

module.exports = class StopCommand extends Command {
	constructor() {
		super({
			name: 'stop',
			description: 'Stop all music and empty the queue.',
			guildOnly: true,
			aliases: ['skipall', 'end']
		});
	}

	run(message) {
		if (message.channel.permissionsFor(message.member).has('MANAGE_GUILD') || message.client.isOwner(message.author.id)) {
			const voiceChannel = message.guild.me.voiceChannel;
			if (!voiceChannel) return error('I can\'t end the music if i\'m not in a voice channel.', message);
			if (!voiceChannel.members.has(message.author.id)) return error('You\'re not in the voice channel playing music.', message);

			if (message.guild.voiceConnection.dispatcher) {
				message.channel.send('Ending music and leaving channel. Thanks for listening!');
				message.guild.voiceConnection.dispatcher.end('endAll');
				message.client.playlists.delete(message.guild.id);
				voiceChannel.leave();
			} else {
				error('I\'m not playing anything?', message);
			}
		} else {
			return error('You do not have permission to end the music.\nYou need the manage guild permission.', message);
		}
	}
};
