const { Command } = require('smooth-discord.js');

module.exports = class ServerInfoCommand extends Command {
	constructor() {
		super({
			name: 'serverinfo',
			description: 'Displays information on the server.',
			guildOnly: true,
			ownerOnly: true,
			aliases: ['si'],
			perms: ['EMBED_LINKS']
		});
	}

	run(message, args) {

	}
};
