const Discord = require('discord.js');
const fs = require('fs');
const path = require('path');
const toDoPath = path.join(__dirname, 'todo.json');

function findUser(message, args) {
	return message.mentions.users.first()
		|| message.client.users.get(args)
		|| message.client.users.find(userino => userino.username.toLowerCase().includes(args.toLowerCase()));
}

function error(errorText, message) {
	const embed = new Discord.RichEmbed();
	embed.setColor(0xee3737)
		.setDescription(`:x: ${errorText}`);
	message.channel.send({ embed });
}

function readToDo() {
	return JSON.parse(fs.readFileSync(toDoPath), 'utf-8');
}

function writeToDo(obj) {
	return fs.writeFile(toDoPath, JSON.stringify(obj), err => { if (err) console.error(err); });
}

exports.findUser = findUser;
exports.error = error;
exports.readToDo = readToDo;
exports.writeToDo = writeToDo;
