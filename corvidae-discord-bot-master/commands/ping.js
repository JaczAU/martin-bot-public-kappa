const { Command } = require('smooth-discord.js');

module.exports = class PingCommand extends Command {
	constructor() {
		super({
			name: 'ping',
			description: 'Test command for the corvidae bot.',
			guildOnly: true,
			ownerOnly: true
		});
	}

	run(message) {
		message.channel.send(`Pong: **${message.author.username}**`);
	}
};
