const { Command } = require('smooth-discord.js');
const Playlist = require('../models/Playlist.js');
const { error } = require('../util.js');

module.exports = class PlaylistsCommand extends Command {
	constructor() {
		super({
			name: 'playlists',
			description: 'Display all the playlists.'
		});
	}

	run(message) {
		Playlist.findAll().then(playlists => {
			if (!playlists.length) return error(`There are no playlists at all!\n\nCreate the first one with \`${message.client.prefix}playlist-create <playlistname>\``, message);

			let playlistsText = '__**Playlists**__\n\n';

			playlists.forEach(pl => playlistsText += ` - **${pl.name}** (created by **${message.client.users.get(pl.authorId).tag}**)\n`);

			message.channel.send(`Sent you the playlist info **${message.author.username}**!`);
			message.author.send(playlistsText, { split: true });
		});
	}
};
