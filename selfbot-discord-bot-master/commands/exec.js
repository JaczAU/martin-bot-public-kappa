const { Command } = require('smooth-discord.js');
const { exec } = require('child_process');

module.exports = class ExecCommand extends Command {
	constructor() {
		super({
			name: 'exec',
			description: 'Executes cli.',
			guildOnly: false
		});
	}

	run(message, args) {
		return exec(args, (err, stdout) => {
			if (err) return message.channel.send(err.message, { code: true });
			return message.channel.send(stdout, { code: true });
		});
	}
};
