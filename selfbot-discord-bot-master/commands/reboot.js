const { Command } = require('smooth-discord.js');

module.exports = class RebootCommand extends Command {
	constructor() {
		super({
			name: 'reboot',
			description: 'Reboots the bot.',
			guildOnly: false
		});
	}

	run(message, args) {
		message.edit('Rebooting...').then(() => {
			setTimeout(() => {
				process.exit();
			}, 3000);
		});
	}
};
