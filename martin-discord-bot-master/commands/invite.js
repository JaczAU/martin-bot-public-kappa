const { Command } = require('smooth-discord.js');
const { stripIndents } = require('common-tags');

module.exports = class InviteCommand extends Command {
	constructor() {
		super({
			name: 'invite',
			description: 'Gives you the invite link and the offical martin discord server.'
		});
	}

	run(message) {
		message.channel.send(stripIndents`
			Here is an invite link to add me to your server: **<https://discordapp.com/oauth2/authorize?permissions=8&scope=bot&client_id=303762262060040196>**

			If you have any questions or need help, join my discord help server: **<https://discord.gg/JPbeFvC>**
		`);
	}
};
