const { Command } = require('smooth-discord.js');
const Canvas = require('canvas');
const { findUser, error } = require('../util.js');
const { get } = require('snekfetch');
const path = require('path');

module.exports = class WizardCommand extends Command {
	constructor() {
		super({
			name: 'wizard',
			description: 'canvas wizard command',
			guildOnly: true,
			aliases: ['wanted'],
			perms: ['ATTACH_FILES']
		});
	}

	run(message, args) {
		if (!args) return this._drawImage(message, message.author);
		const user = findUser(message, args);

		if (user === null) return error('Please input a valid user', message);
		const member = message.guild.members.get(user.id);

		if (member === undefined) return error('User is in a different server', message);
		return this._drawImage(message, user);
	}

	_drawImage(message, user) {
		get(user.avatarURL).then(res => {
			const canvas = new Canvas(354, 491);
			const ctx = canvas.getContext('2d');
			const { Image } = Canvas;

			const background = new Image();
			background.src = path.join(__dirname, '..', 'images', 'wanted.jpg');

			const avatar = new Image();
			avatar.src = res.body;

			ctx.drawImage(background, 0, 0);
			ctx.drawImage(avatar, 60, 148, 237, 237);

			message.channel.send({ files: [{ attachment: canvas.toBuffer() }] });
		});
	}
};
