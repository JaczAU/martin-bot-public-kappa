const { Command } = require('smooth-discord.js');
const { get } = require('snekfetch');
const Canvas = require('canvas');
const { twitch } = require('../config.json');

module.exports = class StreamCommand extends Command {
	constructor() {
		super({
			name: 'stream',
			description: 'Show info on a stream.',
			guildOnly: false,
			ownerOnly: true,
			aliases: ['streamer'],
			perms: ['ATTACH_FILES']
		});
	}

	run(message, args) {
		get(`https://api.twitch.tv/kraken/streams/${args.toLowerCase()}?client_id=${twitch}`).then(res => {
			if (!res.body.stream) return message.channel.send(`Sorry ${message.author.username} this person isn't streaming.`);

			const canvas = new Canvas(550, 400);
			const ctx = canvas.getContext('2d');
			const { Image } = Canvas;

			const grd = ctx.createLinearGradient(0, 0, 0, canvas.height);
			grd.addColorStop(0, '#292929');
			grd.addColorStop(1, '#636262');

			ctx.fillStyle = grd;
			ctx.fillRect(0, 0, canvas.width, canvas.height);

			ctx.fillStyle = '#ffffff';
			ctx.fillRect(16.5, 16.5, 517, 293);

			ctx.fillRect(16.5, 321, 517, 62.5);

			ctx.fillStyle = '#0b0b0b';
			ctx.font = '26px Arial';
			ctx.fillText(res.body.stream.channel.display_name, 30, 350);

			ctx.font = '16px Arial';
			ctx.fillText(res.body.stream.game, 30, 375);

			ctx.font = '14px Arial';
			ctx.fillText(res.body.stream.channel.status.replace(/(.{30})/g, "$1\n"), 270, 342);

			get(res.body.stream.preview.large).then(res => {
				const image = new Image();
				image.src = res.body;
				ctx.drawImage(image, 19, 19, 512, 288);
				message.channel.send({ files: [{ attachment: canvas.toBuffer() }] });
			});

		});
	}
};