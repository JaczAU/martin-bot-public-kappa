const { Command } = require('smooth-discord.js');
const { stripIndents } = require('common-tags');

module.exports = class PingCommand extends Command {
	constructor() {
		super({
			name: 'ping',
			description: 'Gets ping yo',
			aliases: ['ms']
		});
	}

	run(message) {
		message.channel.send('Pinging...').then(msg => {
			msg.edit(stripIndents`
				__**Calculated Ping**__
				**Client:** ${Math.round(message.client.ping)}ms
				**Round trip:** ${msg.createdTimestamp - message.createdTimestamp}ms
			`);
		});
	}
};
