const { Command } = require('smooth-discord.js');
const { error } = require('../util.js');
const { Util } = require('discord.js');

module.exports = class FindPlayingCommand extends Command {
	constructor() {
		super({
			name: 'findplaying',
			description: 'See who is playing.',
			guildOnly: true,
			aliases: ['whosplaying']
		});
	}

	run(message, args) {
		if (!args) return error('You didn\'t specify a game', message);

		const players = message.guild.members.filter(mem => mem.user.presence.game && mem.user.presence.game.name === args);

		if (!players.size) return message.reply(`No one is playing **${args}**`);

		let text = `Users playing **${args}**\n\n`;

		players.forEach(player => {
			text += ` ● **${Util.escapeMarkdown(player.user.username)}**\n`;
		});

		message.edit(text);
	}
};
