const { Command } = require('smooth-discord.js');

module.exports = class RebootCommand extends Command {
	constructor() {
		super({
			name: 'reboot',
			description: 'Reboots the bot.',
			aliases: ['restart', 'shutdown'],
			ownerOnly: true
		});
	}

	run(message) {
		message.delete();
		message.channel.send('Rebooting...').then(msg => {
			setTimeout(() => {
				msg.delete();
			}, 2000);
			setTimeout(() => {
				process.exit();
			}, 3000);
		});
	}
};
