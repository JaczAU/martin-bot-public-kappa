const { User } = require('discord.js');

Object.defineProperty(User.prototype, 'points', {
	get: function() { return client.points.get(this.id); }, // eslint-disable-line
	set: function(value) { client.points.set(this.id, value); } // eslint-disable-line
});

const MartinClient = require('./struct/client.js');
const path = require('path');
const { token } = require('./info.json');
const db = require('./providers/sqlite.js');
const api = require('./providers/api.js');
const Users = require('./models/User.js');
const earnedRecently = [];

const client = new MartinClient({
	owners: ['189696688657530880'],
	prefix: 'm)',
	commandDirectory: path.join(__dirname, 'commands'),
	errorResponse: true,
	debug: true
});

client.once('ready', () => {
	api.initAPI(client);

	client.user.setGame(`${client.prefix}help | martin.artful-is-la.me`);

	console.log(`Logged in as ${client.user.tag} spying on ${client.guilds.size} guilds with ${client.channels.size} channels and ${client.users.size} users`);

	db.sync().then(() => {
		Users.findAll().then(users => {
			users.forEach(user => {
				const person = client.users.get(user.UserId);
				if (person) person.points = user.Points;
			});
		});
	});

	setInterval(() => {
		client.users.forEach(user => {
			if (user.points) {
				Users.upsert({ UserId: user.id, Points: user.points });
			}
		});
	}, 600000);
});

client.on('message', message => {
	if (message.channel.type === 'dm') return;
	if (message.author.bot) return;

	if (!earnedRecently.includes(message.author.id)) {
		if (!message.author.points) message.author.points = 7;
		else message.author.points += 7;

		earnedRecently.push(message.author.id);
		setTimeout(() => {
			earnedRecently.splice(earnedRecently.indexOf(message.author.id), 1);
		}, 8000);
	}
});

client.on('commandError', (command, err) => client.owners.forEach(owner => client.users.get(owner).send(`The **${command.name}** command errored: \`\`\`${err.name}: ${err.message}\`\`\``)));
client.on('voiceStateUpdate', (oldMem, newMem) => require('./events/voiceStateUpdate.js').handle(oldMem, newMem));
client.on('guildMemberAdd', mem => require('./events/guildMemberAdd.js').handle(mem));
client.on('guildMemberRemove', mem => require('./events/guildMemberRemove.js').handle(mem));

client.login(token);
