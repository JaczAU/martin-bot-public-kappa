const { Command } = require('smooth-discord.js');
const { get } = require('snekfetch');
const { RichEmbed } = require('discord.js');

module.exports = class DogFactCommand extends Command {
	constructor() {
		super({
			name: 'dogfact',
			description: 'Get a random dog fact.',
			guildOnly: true,
			aliases: ['randomdogfact'],
			perms: ['EMBED_LINKS']
		});
	}

	async run(message) {
		const { body } = await get('https://dog-api.kinduff.com/api/facts');
		const embed = new RichEmbed().setTitle('Random dog fact!').setColor(0x722aff).setDescription(body.facts[0]);
		message.channel.send({ embed });
	}
};
