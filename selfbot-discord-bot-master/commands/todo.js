const { Command } = require('smooth-discord.js');
const { readToDo, writeToDo, error } = require('../util.js');

module.exports = class ToDoCommand extends Command {
	constructor() {
		super({
			name: 'todo',
			description: 'Use a todo command.'
		});
	}

	run(message, args) {
		const secondaryArgs = args.split(' ').slice(1).join(' ');

		const list = readToDo();

		if (!args) {
			let text = 'To do list: \n\n';

			for (var item in list) {
				text += ` ● ${item}\n`;
			}

			return message.edit(text);
		} else
		if (args.startsWith('add')) {
			if (!list[secondaryArgs]) {
				list[secondaryArgs] = true;

				writeToDo(list);

				return message.edit(`Successfully added: \`${secondaryArgs}\` to to-do list.`);
			} else {
				return error('That is already on the to-do list.', message);
			}
		} else
		if (args.startsWith('remove')) {
			if (list[secondaryArgs]) {
				delete list[secondaryArgs];

				writeToDo(list);

				return message.edit(`Successfully removed: \`${secondaryArgs}\` from to-do list.`);
			} else {
				return error('That is not on the to-do list.', message);
			}
		}
	}
};
