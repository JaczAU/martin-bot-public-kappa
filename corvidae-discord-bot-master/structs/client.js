const { Client } = require('smooth-discord.js');

class CorvidaeClient extends Client {
	constructor(options) {
		super(options);

		this.once('ready', () => console.log(`Logged in as ${this.user.tag}`));
	}

	start(token) {
		return this.login(token);
	}
}

module.exports = CorvidaeClient;
