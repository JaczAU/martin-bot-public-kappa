const db = require('../providers/sqlite.js');
const Sequelize = require('sequelize');

const Playlist = db.define('playlists', {
	name: {
		type: Sequelize.STRING,
		unique: true
	},
	authorId: Sequelize.STRING,
	songsArray: Sequelize.TEXT
});

module.exports = Playlist;
