const { Command } = require('smooth-discord.js');
const Playlist = require('../models/Playlist.js');
const { error } = require('../util.js');

module.exports = class PlaylistCommand extends Command {
	constructor() {
		super({
			name: 'playlist',
			description: 'Display all the songs in your playlist.',
			aliases: ['myp', 'mysongs']
		});
	}

	run(message, args) {
		if (!args) return error('You didn\'t specify a playlist to look at.', message);

		Playlist.findOne({ where: { name: args.toLowerCase() } }).then(playlist => {
			if (!playlist) return error(`There is no playlist called ${args}\nUse \`${message.client.prefix}playlist-create <playlistname>\` to create a playlist.`, message);

			let number = 1;
			let text = `**${message.client.users.get(playlist.authorId).username}'s** playlist:\n\n`;

			JSON.parse(playlist.songsArray).forEach(songObj => {
				text += `${number} - **${songObj.songName}**\n`;
				number++;
			});

			message.channel.send(`Sent you the playlist info **${message.author.username}**!`);
			message.author.send(text, { split: true });
		});
	}
};
