const CorvidaeClient = require('./structs/client.js');
const { token } = require('./config.json');
const { join } = require('path');
const { twitter } = require('./config.json');
const Twitter = require('twitter');
const twitterClient = new Twitter(twitter);
let corvidaeStream;

const client = new CorvidaeClient({
	owners: ['189696688657530880'],
	prefix: '!',
	commandDirectory: join(__dirname, 'commands'),
	debug: true
});

client.once('ready', () => {
	corvidaeStream = twitterClient.stream('statuses/filter', { follow: '4307156655' });

	corvidaeStream.on('data', event => {
		client.channels.get('341236134497615873').send(`**${event.user.screen_name}** posted: ${event.text}`);
	});
});

client.start(token);
