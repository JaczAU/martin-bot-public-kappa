const Discord = require('discord.js');
const { stripIndents } = require('common-tags');
const emojiRegex = require('emoji-regex');

const replaceAll = (message, target, replacement) => {
	return message.split(target).join(replacement);
};

const hook = new Discord.WebhookClient('317858439462256640', 'rysZilVL-Huvuq53I1VQc9yB8WoqFdH5ozTGAGioBEn2va_s1IX7YDsxXlg_KXuXZ6rv');

const emotes = new Map([
	['lenny', '( ͡° ͜ʖ ͡°)'],
	['concern', 'ಠ_ಠ'],
	['fish', '><((((\'>'],
	['sleeping', '(-.-)Zzz...'],
	['table', '(╯°□°）╯︵ ┻━┻'],
	['love', '(✿ ♥‿♥) '],
	['bill', '[̲̅$̲̅(̲̅ιοο̲̅)̲̅$̲̅]'],
	['zoidberg', '(\\/)(Ö,,,,Ö)(\\/) '],
	['yay', '\\(^-^)/'],
	['why', '(ノಠ益ಠ)ノ彡'],
	['sob', '(╥﹏╥)'],
	['tables', '┻━┻︵  \\(°□°)/ ︵ ┻━┻ '],
	['finger', '(ಠ_ಠ)┌∩┐'],
	['thicc', '༼ つ ◕_◕ ༽つ '],
	['hug', '(っ◕‿◕)っ'],
	['smooth', '(づ ￣ ³￣)づ ⓈⓂⓄⓄⓉⒽ'],
	['stroll', 'ᕕ( ᐛ )ᕗ'],
	['kirby', '(つ -‘ _ ‘- )つ'],
	['strong', 'ᕦ(ò_óˇ)ᕤ'],
	['ulenny', '( ͜。 ͡ʖ ͜。)']
]);

function handleEvent(message) {
	if (message.author.bot) return;
	if (message.author.id === message.client.user.id) {
		const emojis = message.content.match(emojiRegex());

		if (emojis) {
			emojis.forEach(emoji => {
				message.react(emoji);
			});
		}

		let eMessage = message.content;

		for (const [key, value] of emotes.entries()) {
			if (eMessage.includes(`.${key}`)) eMessage = replaceAll(eMessage, `.${key}`, value);
		}

		if (eMessage !== message.content) message.edit(Discord.Util.escapeMarkdown(eMessage));
	} else {
		const keywords = ['kye', 'artful'];

		if (keywords.some(word => message.content.toLowerCase().includes(word))) {
			hook.send(stripIndents`
				${message.client.user.toString()}
				User **${message.author.username}** mentioned you in **${message.guild.name}**
				**Channel:** ${message.channel.toString()}
				**Content:**
				${message.content}
			`);
		}
	}
}

exports.handleEvent = handleEvent;
