const { Command } = require('smooth-discord.js');
const snekfetch = require('snekfetch');
const { error } = require('../util.js');
const { RichEmbed } = require('discord.js');

module.exports = class ButtsCommand extends Command {
	constructor() {
		super({
			name: 'butts',
			description: 'Displays a random picture of nsfw butts.',
			guildOnly: false,
			aliases: ['ass'],
			perms: ['EMBED_LINKS']
		});
	}

	run(message) {
		message.delete();
		if (message.channel.nsfw) {
			snekfetch.get('http://api.obutts.ru/butts/0/1/random').then(res => {
				const embed = new RichEmbed().setColor(0x722aff)
					.setImage(`http://media.obutts.ru/${res.body[0].preview}`);
				message.channel.send({ embed });
			});
		} else {
			error('You can only use these commands in NSFW channels.', message);
		}
	}
};
