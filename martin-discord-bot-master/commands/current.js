const { Command } = require('smooth-discord.js');
const { error } = require('../util.js');
const { stripIndents } = require('common-tags');
const { RichEmbed } = require('discord.js');

module.exports = class CurrentCommand extends Command {
	constructor() {
		super({
			name: 'current',
			description: 'Displays the current song playing.',
			guildOnly: true,
			aliases: ['np', 'nowplaying']
		});
	}

	run(message) {
		const voiceChannel = message.guild.me.voiceChannel;
		if (!voiceChannel) return error('I can\'t tell you the songs if i\'m not in a voice channel playing them.', message);

		if (message.guild.voiceConnection.player.dispatcher) {
			const info = message.client.playlists.get(message.guild.id);

			const embed = new RichEmbed()
				.setColor(0x722aff)
				.setDescription(stripIndents`
					**Song:** ${info[0].songName}
					**Length:** ${info[0].length}
					**Queued by:** ${message.client.users.get(info[0].requestedBy)}
				`)
				.setImage(info[0].thumbNail);

			message.channel.send({ embed });
		} else {
			error('I\'m not playing anything!', message);
		}
	}
};
