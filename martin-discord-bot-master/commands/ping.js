const { Command } = require('smooth-discord.js');

module.exports = class PingCommand extends Command {
	constructor() {
		super({
			name: 'ping',
			description: 'Shows the ping of the bot.'
		});
	}

	run(message) {
		message.channel.send('Ping...')
			.then(msg => msg.edit(`Pong! \`${msg.createdTimestamp - message.createdTimestamp}ms\``));
	}
};
