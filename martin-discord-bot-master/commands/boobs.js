const { Command } = require('smooth-discord.js');
const snekfetch = require('snekfetch');
const { error } = require('../util.js');
const { RichEmbed } = require('discord.js');

module.exports = class BoobsCommand extends Command {
	constructor() {
		super({
			name: 'boobs',
			description: 'Displays a random picture of nsfw boobs.',
			guildOnly: false,
			aliases: ['tits'],
			perms: ['EMBED_LINKS']
		});
	}

	run(message) {
		message.delete();
		if (message.channel.nsfw) {
			snekfetch.get('http://api.oboobs.ru/boobs/0/1/random').then(res => {
				const embed = new RichEmbed().setColor(0x722aff)
					.setImage(`http://media.oboobs.ru/${res.body[0].preview}`);
				message.channel.send({ embed });
			});
		} else {
			error('You can only use these commands in NSFW channels.', message);
		}
	}
};
