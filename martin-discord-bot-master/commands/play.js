const { Command } = require('smooth-discord.js');
const YouTube = require('simple-youtube-api');
const ytdl = require('ytdl-core');
const { RichEmbed } = require('discord.js');
const { stripIndents } = require('common-tags');
const token = require('../info.json').keys.google;
const youtube = new YouTube(token);
const { error } = require('../util.js');
const Playlist = require('../models/Playlist.js');

module.exports = class PlayCommand extends Command {
	constructor() {
		super({
			name: 'play',
			description: 'Play audio from a youtube video.',
			guildOnly: true,
			aliases: ['listen'],
			perms: ['EMBED_LINKS']
		});
	}

	run(message, args) {
		if (!args) return error('You didn\'t specify a song to play', message);

		const voiceChannel = message.member.voiceChannel;

		if (!voiceChannel || voiceChannel.type !== 'voice') return error('You\'re not in a voice channel.', message);

		if (args.startsWith('playlist')) {
			const playlistName = args.split(' ').slice(1).join(' ').toLowerCase();

			Playlist.findOne({ where: { name: playlistName } }).then(dbPlaylist => {
				if (!dbPlaylist) return error(`There is no playlist called ${playlistName}\nUse \`${message.client.prefix}playlist-create ${playlistName}\` to create a playlist`, message);

				const playlist = message.client.playlists.get(message.guild.id);

				const arr = JSON.parse(dbPlaylist.songsArray);

				for (let i = arr.length; i; i--) {
					const j = Math.floor(Math.random() * i);
					[arr[i - 1], arr[j]] = [arr[j], arr[i - 1]];
				}

				if (!playlist || playlist.length === 0) {
					message.client.playlists.set(message.guild.id, arr);
					const embed = new RichEmbed()
						.setColor(0x722aff)
						.setDescription(stripIndents`
							Playing **${message.client.users.get(dbPlaylist.authorId)}'s** playlist.
							**Song:** ${arr[0].songName}
							**Length:** ${arr[0].length}
						`)
						.setThumbnail(arr[0].thumbNail);
					message.channel.send({ embed });
					setTimeout(() => this.playSong(message), 1000);
				} else {
					message.client.playlists.set(message.guild.id, playlist.concat(arr));
					const embed = new RichEmbed()
						.setColor(0x722aff)
						.setDescription(stripIndents`
							Adding **${message.client.users.get(dbPlaylist.authorId)}'s** playlist to the queue.
							**Songs:** ${arr.length}
						`);
					message.channel.send({ embed });
				}
			});
		} else {
			youtube.searchVideos(args, 1).then(vid => {
				if (!vid.length) return error('No song found.', message);
				youtube.getVideoByID(vid[0].id).then(res => {
					const url = `https://www.youtube.com/watch?v=${res.id}`;
					const playlist = message.client.playlists.get(message.guild.id);

					const embed = new RichEmbed()
						.setColor(0x722aff)
						.setDescription(stripIndents`
							**Song:** ${res.title}
							**Length:** ${res.duration.minutes}:${res.duration.seconds < 10 ? `0${res.duration.seconds}` : res.duration.seconds} minutes
							**Queued by:** ${message.author}
						`)
						.setThumbnail(`https://img.youtube.com/vi/${res.id}/mqdefault.jpg`);

					const songObj = {
						songName: res.title,
						songURL: url,
						requestedBy: message.author.id,
						length: `${res.duration.minutes}:${res.duration.seconds < 10 ? `0${res.duration.seconds}` : res.duration.seconds} minutes`,
						thumbNail: `https://img.youtube.com/vi/${res.id}/mqdefault.jpg`
					};

					if (!playlist || playlist.length === 0) {
						message.client.playlists.set(message.guild.id, [songObj]);
						message.channel.send({ embed });
						setTimeout(() => this.playSong(message), 1000);
					} else {
						playlist.push(songObj);
						message.channel.send({ embed });
					}
				});
			});
		}
	}

	playSong(message) {
		const playlist = message.client.playlists.get(message.guild.id);
		const nextSong = playlist[0].songURL;

		const stream = ytdl(nextSong, { audioonly: true })
			.once('error', err => {
				console.log(err);
			});

		message.member.voiceChannel.join().then(connection => {
			const dispatcher = connection.playStream(stream, { passes: 1, volume: 0.25 });

			dispatcher.once('end', reason => {
				if (reason !== 'endAll') {
					playlist.shift();

					if (playlist.length !== 0) {
						console.log(`Shifted once in ${message.guild.name}: ${playlist.length} left.`);
						setTimeout(() => this.playSong(message), 1000);
					} else {
						message.channel.send('We\'ve ran out of songs!');
						message.guild.me.voiceChannel.leave();
					}
				}
			});
		}).catch(console.log);
	}
};
