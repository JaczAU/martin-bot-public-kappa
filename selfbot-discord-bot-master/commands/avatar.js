const { Command } = require('smooth-discord.js');
const { findUser, error } = require('../util.js');
const Discord = require('discord.js');
const { embedColour } = require('../info.json');

module.exports = class AvatarCommand extends Command {
	constructor() {
		super({
			name: 'avatar',
			description: 'Shows the avatar of a user.',
			guildOnly: true,
			perms: ['EMBED_LINKS']
		});
	}

	run(message, args) {
		const user = findUser(message, args);

		if (user === null) {
			error('Please input a valid user', message);
		} else {
			const embed = new Discord.RichEmbed();
			embed.setColor(embedColour);
			if (args.length === 0) {
				embed.setImage(message.author.avatarURL);
			} else {
				embed.setImage(user.avatarURL);
			}
			message.edit({ embed });
		}
	}
};
