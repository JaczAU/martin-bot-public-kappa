const { Command } = require('smooth-discord.js');
const teams = new Map([
	['csgo', '311749014636724224']
]);

module.exports = class TeamCommand extends Command {
	constructor() {
		super({
			name: 'team',
			description: 'Show the players in a team.',
			guildOnly: true,
			ownerOnly: true
		});
	}

	run(message, args) {
		if (!args) return message.channel.send('Please specify a team!');

		for (const [key, value] of teams.entries()) {
			if (args === key) {
				const players = message.guild.roles.get(value).members.map(mem => `- **${mem.displayName}**`);
				return message.channel.send(`Here are the **${key}** for Corvidae!\n\n${players.join('\n')}`);
			}
		}

		return message.channel.send('There was no team found!');
	}
};
