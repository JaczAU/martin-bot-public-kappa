const { Command } = require('smooth-discord.js');
const Playlist = require('../models/Playlist.js');
const { error } = require('../util.js');

module.exports = class PlaylistDeleteCommand extends Command {
	constructor() {
		super({
			name: 'playlist-delete',
			description: 'Delete a playlist.'
		});
	}

	run(message, args) {
		if (!args) return error('You did not specify a playlist!', message);

		Playlist.findOne({ where: { name: args } }).then(playlist => {
			if (!playlist) return error(`There is no playlist called **${args}**`, message);
			if (playlist.authorId !== message.author.id) return error(`You cannot delete **${message.client.users.get(playlist.authorId)}**'s playlist!`, message);

			message.channel.send(`Are you sure you want to delete the **${args}** playlist? **yes** or **no**`)
			.then(message.channel.awaitMessages(msg => msg.author.id === message.author.id, { max: 1, time: 5000, errors: ['time'] })
			.then(collected => {
				if (collected.first().content.toLowerCase() === 'yes') {
					playlist.destroy().then(() => {
						message.reply(`Deleted **${playlist.name}**`);
					});
				} else {
					message.channel.send('Cancelling...\nAny answer other than "yes" will cancel this command.').then(msg => msg.delete(5000));
				}
			})
			.catch(() => {
				message.channel.send('Time ran out. Cancelling...').then(msg => msg.delete(5000));
			}));
		});
	}
};
