const { Client } = require('smooth-discord.js');
const path = require('path');
const { token } = require('./info.json');
const client = new Client({
	selfbot: true,
	owners: ['189696688657530880'],
	prefix: 'self.',
	commandDirectory: path.join(__dirname, 'commands'),
	debug: true,
	unkownCommandResponse: true
});

client.once('ready', () => console.log(`Logged in as ${client.user.tag}`));

client.on('message', message => require('./events/message.js').handleEvent(message));

client.login(token);
