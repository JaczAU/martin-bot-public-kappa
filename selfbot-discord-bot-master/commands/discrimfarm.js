const { Command } = require('smooth-discord.js');
const Discord = require('discord.js');
const hook = new Discord.WebhookClient('317858439462256640', 'rysZilVL-Huvuq53I1VQc9yB8WoqFdH5ozTGAGioBEn2va_s1IX7YDsxXlg_KXuXZ6rv');
const acceptableDiscrims = ['0000', '1111', '2222', '3333', '4444', '5555', '6666', '7777', '8888', '9999', '6969', '1337', '1234', '0690', '0069', '6900'];
let timesChanged = 0;

module.exports = class DiscrimCommand extends Command {
	constructor() {
		super({
			name: 'discrimfarm',
			description: 'Farms discriminators.',
			guildOnly: true,
			aliases: ['farm']
		});
	}

	run(message, args) {
		message.channel.send('Farming discrims...');
		farmDiscrim(message);
		message.delete();
	}
};

function farmDiscrim(message) {
	if (timesChanged === 2) {
		timesChanged = 0;
		setTimeout(() => {
			farmDiscrim(message);
		}, 3780000);
		return;
	}
	const currDiscrim = message.client.user.discriminator;
	const nameToChangeTo = message.client.users.filter(user => user.discriminator === currDiscrim && user !== message.client.user).first();
	if (!nameToChangeTo) return hook.send(`${message.client.user.toString()} could not find another user with discrim **${currDiscrim}**`);
	message.client.user.setUsername(nameToChangeTo.username, 'WhiteFan89').then(clientuser => {
		hook.send(`Changed name to **${clientuser.username}** with discrim **${clientuser.discriminator}**`);
		timesChanged++;
		if (acceptableDiscrims.some(discrim => clientuser.discriminator === discrim)) return hook.send(`${clientuser.toString()} found a suitable discrim! ${clientuser.discriminator}`);
		setTimeout(() => {
			farmDiscrim(message);
		}, 10000);
	});
}
