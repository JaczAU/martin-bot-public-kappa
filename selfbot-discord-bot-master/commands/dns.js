const { Command } = require('smooth-discord.js');
const { error } = require('../util.js');
const { lookup } = require('dns');
const { RichEmbed } = require('discord.js');
const { embedColour } = require('../info.json');
const { stripIndents } = require('common-tags');

module.exports = class DNSCommand extends Command {
	constructor() {
		super({
			name: 'dns',
			description: 'Lookup a dns for their ip.',
			aliases: ['lookup']
		});
	}

	run(message, args) {
		if (!args) return error('Specify a doman name.', message);

		lookup(args, (err, address, family) => {
			if (err) return error(err, message);

			const embed = new RichEmbed()
				.setColor(embedColour)
				.setDescription(stripIndents`
					**Domain:** ${args}
					**IP:** ${address}
					**Family:** IPv${family}
				`);

			message.edit({ embed });
		});
	}
};
