const { Command } = require('smooth-discord.js');
const Playlist = require('../models/Playlist.js');
const { error } = require('../util.js');

const replaceAll = (message, target, replacement) => {
	return message.split(target).join(replacement);
};

module.exports = class PlaylistCreateCommand extends Command {
	constructor() {
		super({
			name: 'playlist-create',
			description: 'Create a playlist!'
		});
	}

	run(message, args) {
		if (!args) return error('Please specify a playlist', message);

		const playlistName = replaceAll(args, ' ', '-').toLowerCase();

		Playlist.findAll({ where: { authorId: message.author.id } }).then(ps => {
			console.log(ps.length)
			if (ps.length > 3) return error('You already have 3 playlists. Please delete one to make another.', message);
			Playlist.findOne({ where: { name: playlistName } }).then(playlist => {
				if (!playlist) {
					Playlist.create({ name: playlistName, authorId: message.author.id, songsArray: JSON.stringify([]) }).then(() => {
						message.channel.send(`Created a playlist called **${playlistName}**\n\nUse \`${message.client.prefix}playlist-add ${playlistName} <song-name>\` to add songs to your playlist!`);
					}).catch(err => {
						error('Something went wrong with the databse.', message);
						console.log(err);
					});
				} else {
					error(`There is already a playlist called **${playlistName}**`, message);
				}
			});
		});
	}
};
