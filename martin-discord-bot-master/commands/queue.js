const { Command } = require('smooth-discord.js');

module.exports = class QueueCommand extends Command {
	constructor() {
		super({
			name: 'queue',
			description: 'Displays the current queue.',
			guildOnly: true,
			aliases: ['list']
		});
	}

	run(message) {
		const playlist = message.client.playlists.get(message.guild.id);
		if (!playlist || playlist.length === 0) {
			message.channel.send('There are no songs in the queue!');
		} else {
			let text = `This is the queue for **${message.guild.name}**:\n\n`;
			playlist.forEach(songObj => {
				text += ` - **${songObj.songName}** requested by (**${message.guild.members.get(songObj.requestedBy).displayName}**)\n`;
			});

			message.channel.send(text, { split: true });
		}
	}
};
