const { Command } = require('smooth-discord.js');
const { error } = require('../util.js');
const { Util } = require('discord.js');

module.exports = class FindDiscrimCommand extends Command {
	constructor() {
		super({
			name: 'finddiscrim',
			description: 'Finds the discrim you provide.',
			guildOnly: true,
			aliases: ['whosdiscrim']
		});
	}

	run(message, args) {
		if (args.length !== 4) return error('You\'re args must be the length of 4', message);

		const members = message.client.users.filter(user => user.discriminator === args);

		if (!members.size) return message.reply(`I cannot see anyone with the discrim **${args}**`);

		let text = `Users with the **${args}** discriminator\n\n`;

		members.forEach(user => {
			text += ` ● **${Util.escapeMarkdown(user.tag)}**\n`;
		});

		message.edit(text);
	}
};
