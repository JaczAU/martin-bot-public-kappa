const { Command } = require('smooth-discord.js');
const Playlist = require('../models/Playlist.js');
const { error } = require('../util.js');

module.exports = class PlaylistRemoveCommand extends Command {
	constructor() {
		super({
			name: 'playlist-remove',
			description: 'Remove a song from your playlist by the number in the list.'
		});
	}

	run(message, args) {
		if (!args) return error('You didn\'t specify which song to delete. Use the number next to the song displayed in the playlist command.', message);

		const playlistName = args.split(' ')[0].toLowerCase();
		const songNumber = parseInt(args.split(' ').slice(1));

		Playlist.findOne({ where: { name: playlistName } }).then(playlist => {
			if (!playlist) return error(`There is no playlist called ${playlistName}\nUse \`${message.client.prefix}playlist-create <playlistname>\` to add a playlist.`, message);
			if (playlist.authorId !== message.author.id) return error('You cannot edit other peoples playlists!', message);
			const songs = JSON.parse(playlist.songsArray);

			if (songNumber.isNaN) return error('You didn\'t specify a correct number.', message);
			if (songNumber > songs.length || songNumber < 1) return error(`There is no song in position **${songNumber}**`, message);

			const songName = songs[songNumber - 1].songName;

			songs.splice(songNumber - 1, 1);

			if (songs.length === 0) {
				playlist.destroy();
				message.channel.sendMessage(`Deleted **${playlistName}** due to no songs.`);
			} else {
				playlist.songsArray = JSON.stringify(songs);
				playlist.save().then(() => {
					message.channel.sendMessage(`Successfully deleted **${songName}** from **${playlistName}**.`);
				});
			}
		});
	}
};
