const { Command } = require('smooth-discord.js');
const { embedColour } = require('../info.json');
const { RichEmbed } = require('discord.js');
const snekfetch = require('snekfetch');
const { findUser, error } = require('../util.js');
const { stripIndents } = require('common-tags');

module.exports = class BotsCommand extends Command {
	constructor() {
		super({
			name: 'bots',
			description: 'Shows how many bots a user has made.',
			guildOnly: true,
			perms: ['EMBED_LINKS']
		});
	}

	run(message, args) {
		const user = findUser(message, args);
		if (user === null) return error('Please input a valid user', message);

		const embed = new RichEmbed()
			.setColor(embedColour)
			.setAuthor(user.username, user.avatarURL);

		snekfetch.get('https://menchez.me:2096/bots').then(res => {
			res.body.data.forEach(item => {
				if (item.user.discord_id === user.id) {
					embed.addField(`__${item.name}__`, stripIndents`
						**ID:** ${item.discord_id}
						**Lib:** ${item.library}
						**Description:** ${item.short_description}
					`);
				}
			});

			if (embed.fields.length === 0) error('This user has no bots on magnitex!', message);
			message.channel.send({ embed });
		});
	}
};
