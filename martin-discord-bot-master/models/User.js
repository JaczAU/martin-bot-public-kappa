const db = require('../providers/sqlite.js');
const Sequelize = require('sequelize');

const Users = db.define('users', {
	UserId: {
		type: Sequelize.STRING,
		unique: true
	},
	Points: Sequelize.INTEGER
});

module.exports = Users;
