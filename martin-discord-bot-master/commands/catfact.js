const { Command } = require('smooth-discord.js');
const { get } = require('snekfetch');
const { RichEmbed } = require('discord.js');

module.exports = class CatFactCommand extends Command {
	constructor() {
		super({
			name: 'catfact',
			description: 'Get a random cat fact.',
			guildOnly: true,
			aliases: ['randomcatfact'],
			perms: ['EMBED_LINKS']
		});
	}

	async run(message) {
		const { body } = await get('https://catfact.ninja/fact');
		const embed = new RichEmbed().setTitle('Random cat fact!').setColor(0x722aff).setDescription(body.fact);
		message.channel.send({ embed });
	}
};
