const { Command } = require('smooth-discord.js');
const config = require('../info.json');
const { exec } = require('child_process');
const { URL } = require('url');

const gitURL = new URL(config.gitURL);
gitURL.password = config.gitPassword;
gitURL.username = config.gitUsername;

module.exports = class UpdateCommand extends Command {
	constructor() {
		super({
			name: 'update',
			description: 'Pulls the latest changes from github and reboots the bot.',
			guildOnly: true
		});
	}

	run(message) {
		exec(`git pull ${gitURL}`, (err, stdout) => {
			if (err) return message.channel.send(err.message, { code: true });
			return message.channel.send(stdout, { code: 'fix' }).then(() => {
				process.exit();
			});
		});
	}
};
