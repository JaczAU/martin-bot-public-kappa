const { Command } = require('smooth-discord.js');
const { get } = require('snekfetch');
const { RichEmbed } = require('discord.js');

module.exports = class DogCommand extends Command {
	constructor() {
		super({
			name: 'dog',
			description: 'Display a random dog.',
			guildOnly: true,
			aliases: ['puppy', 'pup'],
			perms: ['EMBED_LINKS']
		});
	}

	async run(message) {
		const { body } = await get('https://random.dog/woof');
		const embed = new RichEmbed().setColor(0x722aff).setImage(`https://random.dog/${body}`);
		message.channel.send({ embed });
	}
};
