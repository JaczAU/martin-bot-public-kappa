const { Command } = require('smooth-discord.js');
const { error } = require('../util.js');

module.exports = class PruneCommand extends Command {
	constructor() {
		super({
			name: 'prune',
			description: 'Delete a certain amount of messages.',
			guildOnly: true,
			aliases: ['delete', 'sanitise', 'purge'],
			perms: ['MANAGE_MESSAGES']
		});
	}

	run(message, args) {
		if (!args) return error('You did not specify a number', message);
		if (isNaN(args)) return error('Please specify a number', message);
		if (!message.channel.permissionsFor(message.member).has('MANAGE_MESSAGES')) return error('You do not have delete message permissions.', message);
		if (parseInt(args) > 97 || parseInt(args) < 1) return error('Define a number between 1 and 97.', message);

		message.delete();

		message.channel.send(`Are you sure you want to delete **${args}** messages? **yes** or **no**`)
			.then(message.channel.awaitMessages(msg => msg.author.id === message.author.id, { max: 1, time: 5000, errors: ['time'] })
			.then(collected => {
				if (collected.first().content.toLowerCase() === 'yes') {
					message.channel.send(`Deleting ${args} messages`).then(() => {
						message.channel.bulkDelete(parseInt(args) + 3, true).catch();
					});
				} else {
					message.channel.send('Cancelling...\nAny answer other than "yes" will cancel this command.').then(msg => msg.delete(5000));
				}
			})
			.catch(() => {
				message.channel.send('Time ran out. Cancelling...').then(msg => msg.delete(5000));
			}));
	}
};
