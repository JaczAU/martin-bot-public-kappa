const { Command } = require('smooth-discord.js');
const { findUser, error } = require('../util.js');

module.exports = class BalanceCommand extends Command {
	constructor() {
		super({
			name: 'balance',
			description: 'Display the balance of a user.',
			aliases: ['bal', 'inv', 'inventory']
		});
	}

	run(message, args) {
		if (!args) {
			message.channel.send(`**${message.author.username}** you have **${message.author.points || 0}** points.`);
		} else {
			const user = findUser(message, args);

			if (!user) return error('You specified an invalid user.', message);

			message.channel.send(`**${message.author.username}** has **${user.points || 0}** points.`);
		}
	}
};
