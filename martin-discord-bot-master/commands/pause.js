const { Command } = require('smooth-discord.js');
const { error } = require('../util.js');

module.exports = class PauseCommand extends Command {
	constructor() {
		super({
			name: 'pause',
			description: 'Pauses the current song.',
			guildOnly: true,
			aliases: ['stop']
		});
	}

	run(message) {
		const voiceChannel = message.guild.me.voiceChannel;
		if (!voiceChannel) return error('I can\'t pause songs if i\'m not in a voice channel.', message);
		if (!voiceChannel.members.has(message.author.id)) return error('You\'re not in the voice channel.', message);

		if (message.guild.voiceConnection.dispatcher) {
			if (message.guild.voiceConnection.dispatcher.paused) {
				message.channel.send('I\'m already paused. Use unpause to start the music again.');
			} else {
				message.channel.send('Pausing...');
				message.guild.voiceConnection.dispatcher.pause();
			}
		} else {
			error('I\'m not playing anything?', message);
		}
	}
};
