const { Command } = require('smooth-discord.js');
const { keys } = require('../info.json');
const { error } = require('../util.js');
const { get } = require('snekfetch');
const Canvas = require('canvas');
const timezone = require('moment-timezone');
const path = require('path');

module.exports = class WeatherCommand extends Command {
	constructor() {
		super({
			name: 'weather',
			description: 'Displays the weather of where you specify.',
			aliases: ['w', 'forecast'],
			perms: ['ATTACH_FILES']
		});
	}

	run(message, args) {
		if (!args) return error('Please specify a place to look for weather', message);

		get(`https://maps.googleapis.com/maps/api/geocode/json?address=${args}&key=${keys.google}`).then(locationRes => {
			if (locationRes.body.status !== 'OK') {
				switch (locationRes.body.status) {
				case 'ZERO_RESULTS':
					return message.channel.sendMessage('No results found');
				case 'REQUEST_DENIED':
					return message.channel.sendMessage('Request denied');
				case 'INVALID_REQUEST':
					return message.channel.sendMessage('Invalid request');
				case 'OVER_QUERY_LIMIT':
					return message.channel.sendMessage('Over limit');
				case 'UNKNOWN_ERROR':
					return message.channel.sendMessage('An unkown error has occured');
				}
			}

			const latitude = locationRes.body.results[0].geometry.location.lat;
			const longitude = locationRes.body.results[0].geometry.location.lng;

			const locality = locationRes.body.results[0].address_components.find(loc => loc.types.includes('locality'));
			const governing = locationRes.body.results[0].address_components.find(gov => gov.types.includes('administrative_area_level_1'));
			const country = locationRes.body.results[0].address_components.find(cou => cou.types.includes('country'));
			const continent = locationRes.body.results[0].address_components.find(con => con.types.includes('continent'));

			const city = locality || governing || country || continent || {};

			get(`https://api.darksky.net/forecast/${keys.darksky}/${latitude},${longitude}`).then(weatherRes => {
				Canvas.registerFont(path.join(__dirname, '..', 'assets', 'fonts', 'NotoSans-Regular.ttf'), { family: 'NotoSans' });
				Canvas.registerFont(path.join(__dirname, '..', 'assets', 'fonts', 'NotoSans-Bold.ttf'), { family: 'NotoSansBold' });

				const canvas = new Canvas(800, 250);
				const ctx = canvas.getContext('2d');
				const Image = Canvas.Image;

				const icon = new Image();
				const background = new Image();

				icon.src = path.join(__dirname, '..', 'assets', 'images', `${getIcon(weatherRes.body.currently.icon)}.png`);
				background.src = path.join(__dirname, '..', 'assets', 'images', 'weatherbg.png');

				ctx.drawImage(background, 0, 0);

				ctx.fillStyle = '#ffffff';
				ctx.font = '22px NotoSans';
				ctx.fillText(city.long_name || 'Unknown', 30, 40);

				ctx.fillStyle = '#ffffff';
				ctx.fillRect(275, 30, 1, canvas.height - 60);

				ctx.fillStyle = '#ffffff';
				ctx.font = '70px NotoSans';
				ctx.fillText(`${convertFToC(weatherRes.body.currently.temperature)}°`, 80, 205);

				ctx.drawImage(icon, 80, 40, 90, 90);

				const timePlacements = [[365, 60], [475, 60], [595, 60], [705, 60]];

				const minPlacements = [[365, 170], [475, 170], [595, 170], [705, 170]];

				const maxPlacements = [[365, 210], [475, 210], [595, 210], [705, 210]];

				const imagePlacements = [[330, 70], [440, 70], [560, 70], [670, 70]];

				for (let i = 0; i < 4; i++) {
					const Time = timezone().tz(weatherRes.body.timezone).add(parseInt(i) + 1, 'days').format('ddd');
					const Min = convertFToC(weatherRes.body.daily.data[i].temperatureMin);
					const Max = convertFToC(weatherRes.body.daily.data[i].temperatureMax);

					const newImage = new Image();
					newImage.src = path.join(__dirname, '..', 'assets', 'images', `${getIcon(weatherRes.body.daily.data[i].icon)}.png`);
					ctx.drawImage(newImage, imagePlacements[i][0], imagePlacements[i][1], 60, 60);

					ctx.textAlign = 'center';
					ctx.fillStyle = '#ffffff';
					ctx.font = '25px NotoSans';
					ctx.fillText(Time, timePlacements[i][0], timePlacements[i][1]);

					ctx.fillStyle = '#ffffff';
					ctx.font = '25px NotoSans';
					ctx.fillText(Min, minPlacements[i][0], minPlacements[i][1]);

					ctx.fillStyle = '#ffffff';
					ctx.font = '25px NotoSans';
					ctx.fillText(Max, maxPlacements[i][0], maxPlacements[i][1]);
				}

				message.channel.send({ files: [{ attachment: canvas.toBuffer() }] });
			}).catch(console.error);
		}).catch(console.eror);
	}
};

function getIcon(icon) {
	if (icon === 'clear-day' || icon === 'partly-cloudy-day') {
		return 'clear';
	} else if (icon === 'clear-night' || icon === 'partly-cloudy-night') {
		return 'night';
	} else if (icon === 'rain' || icon === 'thunderstorm') {
		return 'rain';
	} else if (icon === 'snow' || icon === 'sleet' || icon === 'fog') {
		return 'snow';
	} else if (icon === 'wind' || icon === 'tornado') {
		return 'night';
	} else if (icon === 'cloudy') {
		return 'cloudy';
	} else {
		return 'night';
	}
}

function convertFToC(temp) {
	return Math.round((temp - 32) * 0.5556);
}
