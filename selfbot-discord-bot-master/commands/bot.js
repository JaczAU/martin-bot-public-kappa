const { Command } = require('smooth-discord.js');
const { embedColour } = require('../info.json');
const { RichEmbed } = require('discord.js');
const { stripIndents } = require('common-tags');
const snekfetch = require('snekfetch');

module.exports = class BotCommand extends Command {
	constructor() {
		super({
			name: 'bot',
			description: 'Display a bot from magnitex.',
			guildOnly: true,
			aliases: ['magnitex'],
			perms: ['EMBED_LINKS']
		});
	}

	run(message, args) {
		const embed = new RichEmbed()
			.setColor(embedColour);
		snekfetch.get('https://menchez.me:2096/bots').then(res => {
			res.body.data.forEach(item => {
				embed.addField(`__${item.name}__`, stripIndents`
					**ID:** ${item.discord_id}
					**Lib:** ${item.library}
					**Description:** ${item.short_description}
					**Help Command:** ${item.help_command}
				`);
			});
			message.channel.send({ embed });
		});
	}
};
