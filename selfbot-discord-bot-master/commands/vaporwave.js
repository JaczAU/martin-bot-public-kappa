const { Command } = require('smooth-discord.js');
const { error } = require('../util.js');

module.exports = class VaporwaveCommand extends Command {
	constructor() {
		super({
			name: 'vaporwave',
			description: 'Vaporwave some text.',
			aliases: ['vp']
		});
	}

	run(message, args) {
		if (!args) return error('You didn\'t specify any text', message);

		return message.edit(this._stringToFullWidth(args));
	}

	_charToFullWidth(char) {
		const c = char.charCodeAt(0);
		return c >= 33 && c <= 126
			? String.fromCharCode((c - 33) + 65281)
			: char;
	}

	_stringToFullWidth(string) {
		return string.split('').map(this._charToFullWidth).join('');
	}
};
