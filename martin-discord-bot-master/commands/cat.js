const { Command } = require('smooth-discord.js');
const { get } = require('snekfetch');
const { RichEmbed } = require('discord.js');

module.exports = class CatCommand extends Command {
	constructor() {
		super({
			name: 'cat',
			description: 'Display a random cat.',
			guildOnly: true,
			aliases: ['kitty', 'kitten'],
			perms: ['EMBED_LINKS']
		});
	}

	async run(message) {
		const { body } = await get('http://random.cat/meow');
		const embed = new RichEmbed().setColor(0x722aff).setImage(body.file);
		message.channel.send({ embed });
	}
};
