const { Command } = require('smooth-discord.js');
const { error } = require('../util.js');

module.exports = class PruneCommand extends Command {
	constructor() {
		super({
			name: 'prune',
			description: 'Prunes messages from the channel.',
			aliases: ['delete', 'purge']
		});
	}

	run(message, args) {
		if (!args) return error('Specify a number', message);

		let count = 0;

		message.channel.fetchMessages().then(messages => {
			messages.filter(msg => msg.author.id === message.client.user.id)
				.forEach(messageToDelete => {
					if (count <= args) messageToDelete.delete().then(() => count++);
				});
		});
	}
};
