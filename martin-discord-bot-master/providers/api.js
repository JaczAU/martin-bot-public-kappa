const restify = require('restify');
const rateLimited = [];
const { WebhookClient, RichEmbed } = require('discord.js');
const { stripIndents } = require('common-tags');
const { allTrue } = require('../util.js');
const hook = new WebhookClient('328791527600029696', 'I2mgFldSQNy-fWIf9kvWK_Z539GjUdV-lX5y7pdyjcEQavAbQCkHZmiQ0k7aOBFtV5Av');

const _updateStats = client => {
	return {
		guilds: client.guilds.size,
		users: client.guilds.reduce((p, c) => p + c.memberCount, 0),
		channels: client.channels.size,
		dispatchers: client.voiceConnections.size,
		memUsage: (process.memoryUsage().heapUsed / 1024 / 1024).toFixed(2),
		ping: Math.round(client.ping) || '30'
	};
};

const initAPI = client => {
	const server = restify.createServer({
		name: 'MartinApi'
	});

	server.use(restify.queryParser());

	server.use((req, res, next) => {
		res.header('Access-Control-Allow-Origin', '*');
		res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
		next();
	});

	let stats = _updateStats(client);
	const commands = {};

	setTimeout(() => {
		stats = _updateStats(client);
	}, 10000);

	client.commands.filter(cmd => cmd.ownerOnly !== true).forEach(command => {
		commands[command.name] = command.description;
	});

	server.get('/stats', (req, res, next) => {
		res.send(200, stats);
	});

	server.get('/commands', (req, res, next) => {
		res.send(200, commands);
	});

	server.get('/message', (req, res, next) => {
		if (rateLimited.includes(req.connection.remoteAddress)) return res.send(429, { error: 'Too many requests' });

		const data = req.params;

		if (!allTrue([data.user, data.subject, data.content, data.choice])) return res.send(422, { error: 'Not enough parameters' });

		rateLimited.push(req.connection.remoteAddress);

		setTimeout(() => {
			rateLimited.splice(rateLimited.indexOf(req.connection.remoteAddress), 1);
		}, 1800000);

		const embed = new RichEmbed()
			.setDescription(stripIndents`
				**Name: **${data.user}
				**Subject: **${data.subject}
				**Message: **${data.content}
			`);

		console.log(`Notification: ${data.user}: ${data.subject}: ${data.content}`);
		console.log(`By ${req.connection.remoteAddress}`);

		if (data.choice === '1') embed.setColor(0x52f013);
		else if (data.choice === '2') embed.setColor(0xf06c13);
		else if (data.choice === '3') embed.setColor(0x1391f0);

		hook.send({ embeds: [embed] });

		return res.send(200, 'ok');
	});

	server.listen(8080);
};

module.exports.initAPI = initAPI;
