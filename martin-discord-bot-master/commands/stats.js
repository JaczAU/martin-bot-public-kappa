const { Command } = require('smooth-discord.js');
const { RichEmbed } = require('discord.js');
const { stripIndents } = require('common-tags');
const moment = require('moment');
require('moment-duration-format');

module.exports = class StatsCommand extends Command {
	constructor() {
		super({
			name: 'stats',
			description: 'Shows stats about the bot.',
			aliases: ['statistics', 'info'],
			perms: ['EMBED_LINKS']
		});
	}

	run(message) {
		const embed = new RichEmbed();
		embed.setColor(0x722aff)
			.setTitle('Stats')
			.setThumbnail(message.client.user.avatarURL)
			.addField('__**Martin Stats**__', stripIndents`
				**Uptime:** ${moment.duration(message.client.uptime).format('d[ days], h[ hours], m[ minutes, and ]s[ seconds]')}
				**Memory usage:** ${(process.memoryUsage().heapUsed / 1024 / 1024).toFixed(2)}MB
				**Ping:** ${Math.round(message.client.ping)}ms
				**Guilds:** ${message.client.guilds.size}
				**Users:** ${message.client.guilds.reduce((p, c) => p + c.memberCount, 0)}
			`).addField('__**Creator**__', stripIndents`
				**Github:** [KyeNormanGill](https://github.com/KyeNormanGill)
				**Twitter:** [@GillKye](https://twitter.com/GillKye)
				**Youtube:** [@Artful](https://www.youtube.com/channel/UCNRTR32vS6MoMEudc5VZKng)
			`).addField('__**Dashboard**__', stripIndents`
				**Website:** [martin.artful-is-la.me/](http://martin.artful-is-la.me/)
			`);
		message.channel.send({ embed });
	}
};
