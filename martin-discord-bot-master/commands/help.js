const { Command } = require('smooth-discord.js');
const { RichEmbed } = require('discord.js');

module.exports = class HelpCommand extends Command {
	constructor() {
		super({
			name: 'help',
			description: 'Displays commands.',
			guildOnly: true
		});
	}

	run(message, args) {
		let text = '';
		if (args.length === 0) {
			if (message.client.owners.includes(message.author.id)) {
				text = `The prefix for ${message.client.user.username} is ${message.client.prefix}\nExample: \`${message.client.prefix}help\`\n\n`;

				message.client.commands.forEach(command => {
					text += `**${command.name}:** ${command.description}\n`;
				});

				text += '\n**For more help visit: http://martin.artful-is-la.me/**\n**Or join our discord! https://discord.gg/JPbeFvC**';

				message.author.send(text).then(() => message.channel.send(`**${message.member.displayName}** i've sent you the help list. Check your dms!`));
			} else {
				text = `The prefix for ${message.client.user.username} is ${message.client.prefix}\nExample: \`${message.client.prefix}help\`\n\n`;

				message.client.commands.filter(cmd => cmd.ownerOnly !== true).forEach(command => {
					text += `**${command.name}:** ${command.description}\n`;
				});

				text += '\n**For more help visit: http://martin.artful-is-la.me/**\n**Or join our discord! https://discord.gg/JPbeFvC**';

				message.author.send(text).then(() => message.channel.send(`**${message.member.displayName}** i've sent you the help list. Check your dms!`));
			}
		} else {
			const command = message.client.commands.get(args);

			if (!command) return;

			const embed = new RichEmbed();

			embed.setColor(0x722aff)
				.addField(command.name, command.description);

			message.channel.send({ embed });
		}
	}
};
