const path = require('path');
const Discord = require('discord.js');

function findUser(message, args) {
	return message.client.users.get(args)
		|| message.client.users.find(userino => userino.username.toLowerCase().includes(args.toLowerCase()))
		|| message.mentions.users.first();
}

function findChannel(message, args) {
	return message.mentions.channels.first()
		|| message.guild.channels.get(args)
		|| message.guild.channels.find(channel => channel.name.toLowerCase().includes(args.toLowerCase()));
}

function stripPath(pathName, extension) {
	return path.basename(pathName, `.${extension}`);
}

function error(errorText, message) {
	if (message === undefined) throw Error('Message undefined in error call.');
	const embed = new Discord.RichEmbed();
	embed.setColor(0xee3737)
		.setDescription(`:x: ${errorText}`);
	message.channel.send({ embed });
}

function allTrue(array) {
	return array.every(i => i);
}

exports.allTrue = allTrue;
exports.findUser = findUser;
exports.findChannel = findChannel;
exports.stripPath = stripPath;
exports.error = error;

