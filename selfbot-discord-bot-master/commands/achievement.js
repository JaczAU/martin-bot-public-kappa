const { Command } = require('smooth-discord.js');
const snekfetch = require('snekfetch');
const { error } = require('../util.js');

module.exports = class AchievementCommand extends Command {
	constructor() {
		super({
			name: 'achievement',
			description: 'Minecraft thingy.',
			aliases: ['minecraft', 'mc'],
			perms: ['ATTACH_FILES']
		});
	}

	run(message, args) {
		if (!args) return error('Specify some text ya retard. smh', message);

		if (args.length > 25) return error('Args must be less than or equal to 25 characters', message);

		snekfetch.get(`https://www.minecraftskinstealer.com/achievement/a.php?i=1&h=Achievement%20Get!&t=${args}`).then(res => {
			message.edit({ files: [{ attachment: res.body, name: 'achievement.png' }] });
		});
	}
};
