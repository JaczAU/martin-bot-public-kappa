const { Command } = require('smooth-discord.js');

module.exports = class PrefixCommand extends Command {
	constructor() {
		super({
			name: 'prefix',
			description: 'Show\'s the prefix of the bot'
		});
	}

	run(message, args) {
		return message.channel.send(`${message.client.user.username}'s prefix is \`${message.client.prefix}\``);
	}
};
