const { Command } = require('smooth-discord.js');
const { stripIndents } = require('common-tags');
const moment = require('moment');
require('moment-duration-format');
const Discord = require('discord.js');
const { embedColour } = require('../info.json');

module.exports = class StatsCommand extends Command {
	constructor() {
		super({
			name: 'stats',
			description: 'Statistics on the host.',
			guildOnly: true,
			perms: ['EMBED_LINKS']
		});
	}

	run(message) {
		const embed = new Discord.RichEmbed();
		embed.setColor(embedColour)
			.setTitle('Stats')
			.addField(`__**${message.client.user.username} Stats**__`, stripIndents`
				**Uptime:** ${moment.duration(message.client.uptime).format('d[ days], h[ hours], m[ minutes, and ]s[ seconds]')}
				**Memory usage:** ${(process.memoryUsage().heapUsed / 1024 / 1024).toFixed(2)}MB
				**Ping:** ${Math.round(message.client.ping)}ms
				**Guilds:** ${message.client.guilds.size}
				**Users:** ${message.client.guilds.reduce((p, c) => p + c.memberCount, 0)}
			`)
			.addField('__**Creator**__', stripIndents`
				**Github:** [KyeNormanGill](https://github.com/KyeNormanGill)
				**Twitter:** [@GillKye](https://twitter.com/GillKye)
				**Youtube:** [@Artful](https://www.youtube.com/channel/UCNRTR32vS6MoMEudc5VZKng)
			`);
		message.edit({ embed });
	}
};
