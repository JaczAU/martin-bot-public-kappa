const { Client } = require('smooth-discord.js');
const { Collection } = require('discord.js');

class MartinClient extends Client {
	constructor(options) {
		super(options);

		this.playlists = new Collection();
		this.current = new Collection();
		this.points = new Collection();
	}

	isOwner(id) {
		console.log(id);
		return this.owners.includes(id);
	}
}

module.exports = MartinClient;
